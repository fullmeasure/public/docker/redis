FROM redis:alpine

MAINTAINER FME <dev@fullmeasureed.com>

COPY redis.conf /etc/

CMD [ "redis-server", "/etc/redis.conf" ]